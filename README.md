# README #


### What is this repository for? ###

* Parsing Kennesaw State University Commons Menu site to aggregate food item data
for use in Android and iOS mobile applications.

### Note ###
* Project was initially started in java, but has since been redone and completed in php.
* Application connects to a MySQL DB to store parsed data. This data will then be accessed
via the mobile apps.
