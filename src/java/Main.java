package java;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by Sam.I on 6/1/2015.
 */
public class Main {

    public static void main (String[] args) throws IOException, URISyntaxException {

        String url = "http://dining.kennesawstateauxiliary.com/commonsmenu.htm";

        ParseHtml.parseHtml(url);

    }
}
